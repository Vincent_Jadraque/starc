starc_repo
================

This package allows the Sawyer Research Robot to track AR tag bundles on the STARC test rig.

Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Prerequisites:
Please install the following software:
-------------------------
+------------------+-------------------------------------------------------+
| intera_sdk       | https://github.com/RethinkRobotics/intera_sdk         |
+------------------+-------------------------------------------------------+
| intera_commom    | https://github.com/RethinkRobotics/intera_common      |
+------------------+-------------------------------------------------------+
| sawyer_robot     | https://github.com/RethinkRobotics/sawyer_robot       |
+------------------+-------------------------------------------------------+
| sawyer_moveit    | https://github.com/RethinkRobotics/sawyer_moveit      |
+------------------+-------------------------------------------------------+
| sawyer_simulator | http://sdk.rethinkrobotics.com/intera/Gazebo_Tutorial |
+------------------+-------------------------------------------------------+

Please follow the tutorial_ on the Rethink Robotics Wiki to get started with Sawyer in Gazebo.

.. _tutorial: http://sdk.rethinkrobotics.com/intera/Gazebo_Tutorial

.. image:: sawyer_gazebo/etc/sawyer_gazebo.gif

Code & Tickets
--------------

+-----------------+----------------------------------------------------------------------------------+
| Documentation   | http://sdk.rethinkrobotics.com/intera                                            |
+-----------------+----------------------------------------------------------------------------------+
| Issues          | https://github.com/mmd/sawyer_simulator/issues                       |
+-----------------+----------------------------------------------------------------------------------+
| Contributions   | https://github.com/mmd/sawyer_simulator/blob/master/CONTRIBUTING.md  |
+-----------------+----------------------------------------------------------------------------------+

starc_repo Repository Overview
------------------------------------

::

     .
     |
     +-- robot_setup_tf/                package to broadcast frames linking camera to the Sawyer  
     |
     +-- starc/                         contains the python script for Sawyer to track the AR tag bundles

Other Sawyer Repositories
-------------------------
+------------------+-------------------------------------------------------+
| intera_sdk       | https://github.com/RethinkRobotics/intera_sdk         |
+------------------+-------------------------------------------------------+
| intera_commom    | https://github.com/RethinkRobotics/intera_common      |
+------------------+-------------------------------------------------------+
| sawyer_robot     | https://github.com/RethinkRobotics/sawyer_robot       |
+------------------+-------------------------------------------------------+
| sawyer_moveit    | https://github.com/RethinkRobotics/sawyer_moveit      |
+------------------+-------------------------------------------------------+
| sawyer_simulator | http://sdk.rethinkrobotics.com/intera/Gazebo_Tutorial |
+------------------+-------------------------------------------------------+

Latest Release Information
--------------------------

http://sdk.rethinkrobotics.com/intera/Release-Changes
