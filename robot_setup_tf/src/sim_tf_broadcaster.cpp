// Author: Vincent Jadraque
// Student ID: 11970720
// Date: 14/11/2018

// File name: sim_tf_broadcaster.cpp
/* 
   Description: 

for simulation - broadcasts hardcoded transform between 
the base frame and the camera frame
also adds target frame at ar marker frame with roll of 180 degrees 

*/

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>

int id = 0;

// callback to get frame id from ar_pose_marker topic
void cbGetMarkerFrame(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& msg)
{
  // retrieve the frame id from the first marker in the message
  // TODO: loop through markers and search for correct marker.id
  
  try
  {
    id = msg->markers.at(0).id;
    ROS_INFO("ID is %d", id);
  }
  catch (std::out_of_range oor) {
    ROS_ERROR("%s", oor.what());
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "robot_tf_publisher");
  ros::NodeHandle n;

  ros::Rate r(100);

  tf::TransformBroadcaster broadcaster;
  tf::TransformListener listener;
  ros::Subscriber sub = n.subscribe("ar_pose_marker", 1, cbGetMarkerFrame);

  while(n.ok()){

    broadcaster.sendTransform(
      tf::StampedTransform(
        // quat - xyzw; translation - xyz
        // quaternion changes from base orientation (z is upwards) to camera (z points to front of Sawyer)
        // translation is (0.0, 0.4, 0.3) (xyz) away from the base
        tf::Transform(tf::Quaternion(-0.5, 0.5, -0.5, 0.5), tf::Vector3(0.0, 0.4, 0.3)),
        ros::Time::now(),"base", "usb_cam"));

    try
    {
      ros::spinOnce();
      std::string frame_id = "ar_marker_" + boost::lexical_cast<std::string>(id);

      broadcaster.sendTransform(
      tf::StampedTransform(
        // quat - xyzw; translation - xyz
        // 180 degree rotation to make target pose face the ar tag
        tf::Transform(tf::Quaternion(1.0, 0.0, 0.0, 0.0), tf::Vector3(0.0, 0.0, 0.0)),
        // TODO: make code more robust by automating the marker frame id - make callback subscriber
        ros::Time::now(), frame_id, "target"));
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    r.sleep();
  }
}
