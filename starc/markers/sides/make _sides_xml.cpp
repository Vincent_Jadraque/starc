// Author: Vincent Jadraque
// Student ID: 11970720
// Date: 29/10/2018

// File name: make_marker_xml.cpp
/* 
   Description: 

automatically calculates the positions of the AR tags and
prints out xml file of AR tags on perimeter of the test rig.

*/

#include <iostream>
#include <vector>
#include <math.h>

#define PI 3.14159265358979323

using namespace std;

// TODO: add flexibility to handle different number of tags per cube face
// TODO: make marker class


// Make struct point - each ordinate x, y, z are doubles
struct point {
  double x;
  double y;
  double z;
};

// Make struct marker - c br tr tl
// Each AR marker is defined by their centre point, 
// bottom right, top right and top left corners
struct marker {
  // centre
  point c;
  // bottom right
  point br;
  // top right
  point tr;
  // top left
  point tl;
};

point getBottomRight(point centre, double hl) {
  if (centre.x == hl) {
    
  }
}

point rotatePointAroundZ(point original_point, double degrees) {
  
  int angle = degrees/90.0;
  double cos_theta = 0.0;
  double sin_theta = 0.0;
  switch (angle)
  {
    // if degrees is -90
    case -1: sin_theta = -1.0; break;
    // if degrees is 90
    case 1: sin_theta = 1.0; break;
    // if degrees is 180
    case 2: cos_theta = -1.0; break;
    default: break;
  }

  point result;
  result.x = original_point.x*cos_theta - original_point.y*sin_theta;
  result.y = original_point.y*cos_theta + original_point.x*sin_theta;
  result.z = original_point.z;
  return result;
}


// degrees is angle in degrees
vector<marker> rotateBracketOfMarkers(vector<marker> original_markers, double degrees) {
  // initialise vector of markers to be returned by function
  vector<marker> result;

//For loop through all markers

  for(int i = 0; i<4*original_markers.size(); i++) {
    marker rotated_marker;
    rotated_marker.c = rotatePointAroundZ(original_markers.at(i).c, degrees);
    rotated_marker.br = rotatePointAroundZ(original_markers.at(i).br, degrees);
    rotated_marker.tr = rotatePointAroundZ(original_markers.at(i).tr, degrees);
    rotated_marker.tl = rotatePointAroundZ(original_markers.at(i).tl, degrees);
  }

    /*
      BR
      TR
      TL
    */
}

/*
yMarkers = rBOM(original_markers, 90);
negXMarkers = rBOM(original_markers, 180);
negYMarkers = rBOM(original_markers, -90);
    for(int i = 0; i<4*original_markers.size(); i++)
    printMarker(original_markers.at(i));
*/

void printPoint (point point) {
  cout << "        <corner x=\"" << point.x << "\" y=\"" << point.y << "\" z=\"" << point.z << "\" />" << endl;
}

// print positions of markers' points
void printMarker (marker marker) {
  printPoint(marker.c);
  printPoint(marker.br);
  printPoint(marker.tr);
  printPoint(marker.tl);
}

int main (int argc, char** argv) {
  int num_tags = 20;
  double b_length = 413.46;
  double b_width = 71;
  double theta = 30;
  double ar_size = 4;

  // half ar size
  double ha = ar_size/2;
  // calculated positions
  double hl = b_length/2 + ha*cos(theta);
  double spacing = (length - 2*ar_size)/3;
  double a = spacing/2;
  double b = ar_size/6 + d/3;
  double c = 2*ar_size/3 + d/3;
  int index = 1;

/*
  Vector of markers 
  
  Functions to getBottomRight, getTopRight, getTopLeft(centre)
  if x/y==hl

  Function to print marker - takes in 4 points
  Function to translate that marker in x direction
  Function to rotate a vector of markers


  Initialise bracket length =, width, square, theta
  Spacing between markers = 8
  hs = 0.5*square;

  Initilise centre point of 1st marker (25) (x+)


  getBR, TR, TL

  Initialise marker.centre = 2*spacing, hs, b_width*cos(theta)
  Marker25.br = getBR(marker.centre)
  Tr=....
  Tl=....


  xMarkers 
    for(int i = 0; i<4*xMarkers.size(); i++)
    xMarkers.at(i) = translateX(marker25, spacing);
*/

  cout << "<multimarker markers=\"" << num_tags << "\">" << endl;
  
  // begin printing markers
  // Face 1 (x)
  // Marker 1
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(d,b,b, d,c,a, d,c,c, d,a,c);
  cout << "    </marker>" << endl;

  // Marker 2
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(d,b,-b, d,c,-c, d,c,-a, d,a,-a);
  cout << "    </marker>" << endl;
  
  // Marker 3
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(d,-b,-b, d,-a,-c, d,-a,-a, d,-c,-a);
  cout << "    </marker>" << endl;
  
  // Marker 4
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(d,-b,b, d,-a,a, d,-a,c, d,-c,c);
  cout << "    </marker>" << endl;
  
  // Face 2 (y)
  // Marker 5
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-b,d,b, -c,d,a, -c,d,c, -a,d,c);
  cout << "    </marker>" << endl;
  
  // Marker 6
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-b,d,-b, -c,d,-c, -c,d,-a, -a,d,-a);
  cout << "    </marker>" << endl;
  
  // Marker 7
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(b,d,-b, a,d,-c, a,d,-a, c,d,-a);
  cout << "    </marker>" << endl;
  
  // Marker 8
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(b,d,b, a,d,a, a,d,c, c,d,c);
  cout << "    </marker>" << endl;
  
  // Face 3 (z)
  // Marker 9
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-b,b,d, -a,c,d, -c,c,d, -c,a,d);
  cout << "    </marker>" << endl;
  
  // Marker 10
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(b,b,d, c,c,d, a,c,d, a,a,d);
  cout << "    </marker>" << endl;
  
  // Marker 11
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(b,-b,d, c,-a,d, a,-a,d, a,c,d);
  cout << "    </marker>" << endl;
  
  // Marker 12
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-b,-b,d, -a,-a,d, -c,-a,d, -c,c,d);
  cout << "    </marker>" << endl;
  
  // Face 4 (-x)
  // Marker 13
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-d,-b,b, -d,-c,a, -d,-c,c, -d,-a,c);
  cout << "    </marker>" << endl;
  
  // Marker 14
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-d,-b,-b, -d,-c,-c, -d,-c,-a, -d,-a,-a);
  cout << "    </marker>" << endl;
  
  // Marker 15
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-d,b,-b, -d,a,-c, -d,a,-a, -d,c,-a);
  cout << "    </marker>" << endl;
  
  // Marker 16
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-d,b,b, -d,a,a, -d,a,c, -d,c,c);
  cout << "    </marker>" << endl;
  
  // Face 5 (-y)
  // Marker 17
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(b,-d,b, c,-d,a, c,-d,c, a,-d,c);
  cout << "    </marker>" << endl;
  
  // Marker 6
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(b,-d,-b, c,-d,-c, c,-d,-a, a,-d,-a);
  cout << "    </marker>" << endl;
  
  // Marker 7
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-b,-d,-b, -a,-d,-c, -a,-d,-a, -c,-d,-a);
  cout << "    </marker>" << endl;
  
  // Marker 8
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-b,-d,b, -a,-d,a, -a,-d,c, -c,-d,c);
  cout << "    </marker>" << endl;
  
  // Face 6 (-z)
  // Marker 21
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(b,b,-d, b-ha,b+ha,-d, b+ha,b+ha,-d, b+ha,b-ha,-d);
  cout << "    </marker>" << endl;
  
  // Marker 22
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-b,b,-d, -b-ha,b+ha,-d, -b+ha,b+ha,-d, -b+ha,b-ha,-d);
  cout << "    </marker>" << endl;
  
  // Marker 23
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(-b,-b,-d, -b-ha,-b+ha,-d, -b+ha,-b+ha,-d, -b+ha,-b-ha,-d);
  cout << "    </marker>" << endl;
  
  // Marker 24
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  printMarker(b,-b,-d, b-ha,-b+ha,-d, b+ha,-b+ha,-d, b+ha,-b-ha,-d);
  cout << "    </marker>" << endl;
  
  cout << "</multimarker>\n";
}