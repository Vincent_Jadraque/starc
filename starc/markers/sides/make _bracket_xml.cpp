// Author: Vincent Jadraque
// Student ID: 11970720
// Date: 29/10/2018

// File name: make_marker_xml.cpp
/* 
   Description: 

automatically calculates the positions of the AR tags and
prints out xml file of AR cube.

*/

#include <iostream>
using namespace std;

// TODO: add flexibility to handle different number of tags per cube face
// TODO: make marker class


/*
   ADDED CODE FROM PHONE
   Vector of markers
*/

/*

  Make struct point - xyz
  Make struct marker - c br tr tl
  Vector of markers 
  
  Functions to getBottomRight, getTopRight, getTopLeft(centre)
  if x/y==hs

  Function to print marker - takes in 4 points
  Function to translate that marker in x direction
  Function to rotate a vector of markers


  Initialise bracket length =, width, square, theta
  Spacing between markers = 8
  hs = 0.5*square;

  Initilise centre point of 1st marker (25) (x+)


  getBR, TR, TL

  Initialise marker.centre = 2*spacing, hs, b_width*cos(theta)
  Marker25.br = getBR(marker.centre)
  Tr=....
  Tl=....


  xMarkers 
    for(int i = 0; i<4*xMarkers.size(); i++)
    xMarkers.at(i) = translateX(marker25, spacing);
*/


Vector<marker> rotateBracketOfMarkers(vector<marker> original_markers, angle) {

//For loop through all markers

  for(int i = 0; i<4*xMarkers.size(); i++)
  Result.at(i) centre = rotatePointAroundZ(OM.at(i).centre, angle);

    yMarkers = rBOM(xMarkers, 90);
    negXMarkers = rBOM(xMarkers, 180);
    negYMarkers = rBOM(yMarkers, -90); // (code changed from) negYMarkers = rBOM(xMarkers, -90); 
    /*BR
      TR
      TL
    */

    for(int i = 0; i<4*xMarkers.size(); i++)
    PrintMarker(xMarkers.at(i));

}

void print_corner (double x1, double y1, double z1) {
  cout << "        <corner x=\"" << x1 << "\" y=\"" << y1 << "\" z=\"" << z1 << "\" />" << endl;
}

void print_marker_positions (double x1, double y1, double z1, double x2, double y2, double z2, 
                             double x3, double y3, double z3, double x4, double y4, double z4) {
// void print_marker_positions (double x1, double y1, double z1) {
  print_corner(x1, y1, z1);
  print_corner(x2, y2, z2);
  print_corner(x3, y3, z3);
  print_corner(x4, y4, z4);
}


// currently hardcoded to have only 4 tags per face
int main (int argc, char** argv) {
  int num_tags = 24;
  double length = 9;
  double ar_size = 3;

  // half ar size
  double ha = ar_size/2;
  // calculated positions
  double d = length/2;
  double spacing = (length - 2*ar_size)/3;
  double a = spacing/2;
  double b = ar_size/6 + d/3;
  double c = 2*ar_size/3 + d/3;
  int index = 1;

  cout << "<multimarker markers=\"" << num_tags << "\">" << endl;
  
  // begin printing markers
  // Face 1 (x)
  // Marker 1
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(d,b,b, d,c,a, d,c,c, d,a,c);
  cout << "    </marker>" << endl;

  // Marker 2
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(d,b,-b, d,c,-c, d,c,-a, d,a,-a);
  cout << "    </marker>" << endl;
  
  // Marker 3
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(d,-b,-b, d,-a,-c, d,-a,-a, d,-c,-a);
  cout << "    </marker>" << endl;
  
  // Marker 4
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(d,-b,b, d,-a,a, d,-a,c, d,-c,c);
  cout << "    </marker>" << endl;
  
  // Face 2 (y)
  // Marker 5
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-b,d,b, -c,d,a, -c,d,c, -a,d,c);
  cout << "    </marker>" << endl;
  
  // Marker 6
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-b,d,-b, -c,d,-c, -c,d,-a, -a,d,-a);
  cout << "    </marker>" << endl;
  
  // Marker 7
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(b,d,-b, a,d,-c, a,d,-a, c,d,-a);
  cout << "    </marker>" << endl;
  
  // Marker 8
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(b,d,b, a,d,a, a,d,c, c,d,c);
  cout << "    </marker>" << endl;
  
  // Face 3 (z)
  // Marker 9
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-b,b,d, -a,c,d, -c,c,d, -c,a,d);
  cout << "    </marker>" << endl;
  
  // Marker 10
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(b,b,d, c,c,d, a,c,d, a,a,d);
  cout << "    </marker>" << endl;
  
  // Marker 11
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(b,-b,d, c,-a,d, a,-a,d, a,c,d);
  cout << "    </marker>" << endl;
  
  // Marker 12
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-b,-b,d, -a,-a,d, -c,-a,d, -c,c,d);
  cout << "    </marker>" << endl;
  
  // Face 4 (-x)
  // Marker 13
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-d,-b,b, -d,-c,a, -d,-c,c, -d,-a,c);
  cout << "    </marker>" << endl;
  
  // Marker 14
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-d,-b,-b, -d,-c,-c, -d,-c,-a, -d,-a,-a);
  cout << "    </marker>" << endl;
  
  // Marker 15
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-d,b,-b, -d,a,-c, -d,a,-a, -d,c,-a);
  cout << "    </marker>" << endl;
  
  // Marker 16
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-d,b,b, -d,a,a, -d,a,c, -d,c,c);
  cout << "    </marker>" << endl;
  
  // Face 5 (-y)
  // Marker 17
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(b,-d,b, c,-d,a, c,-d,c, a,-d,c);
  cout << "    </marker>" << endl;
  
  // Marker 6
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(b,-d,-b, c,-d,-c, c,-d,-a, a,-d,-a);
  cout << "    </marker>" << endl;
  
  // Marker 7
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-b,-d,-b, -a,-d,-c, -a,-d,-a, -c,-d,-a);
  cout << "    </marker>" << endl;
  
  // Marker 8
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-b,-d,b, -a,-d,a, -a,-d,c, -c,-d,c);
  cout << "    </marker>" << endl;
  
  // Face 6 (-z)
  // Marker 21
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(b,b,-d, b-ha,b+ha,-d, b+ha,b+ha,-d, b+ha,b-ha,-d);
  cout << "    </marker>" << endl;
  
  // Marker 22
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-b,b,-d, -b-ha,b+ha,-d, -b+ha,b+ha,-d, -b+ha,b-ha,-d);
  cout << "    </marker>" << endl;
  
  // Marker 23
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(-b,-b,-d, -b-ha,-b+ha,-d, -b+ha,-b+ha,-d, -b+ha,-b-ha,-d);
  cout << "    </marker>" << endl;
  
  // Marker 24
  cout << "    <marker index=\"" << index++ << "\" status=\"1\">" << endl;
  print_marker_positions(b,-b,-d, b-ha,-b+ha,-d, b+ha,-b+ha,-d, b+ha,-b-ha,-d);
  cout << "    </marker>" << endl;
  
  cout << "</multimarker>\n";
  // for (int i = 0; i < 1, i++) {
  // }
}